package kapitel1;

//Aufgabe 1.4

class Asterisk
{
	public static void main( String[] args )
	{
		// Teil a)
		System.out.println("\na. ");
		System.out.print("*\n**\n***\n****\n*****");

		System.out.println("\n\nb. ");
		System.out.println("*");
		System.out.println("***");
		System.out.println("*****");
		System.out.println("****");
		System.out.println("**");
		
		System.out.println("\nc. ");		
		System.out.print("*");
		System.out.print("***"); 
		System.out.print("*****"); 
		System.out.print("****"); 
		System.out.println("**");
		
		System.out.println("\nd. ");
		System.out.print("*");
		System.out.println("***");
		System.out.println("*****");
		System.out.print("****");
		System.out.println("**");	
	}
}