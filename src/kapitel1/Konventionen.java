package kapitel1;
/*
	Example: konventionen.java
Ausgabe der URL f�r die Java Code Conventions
Klasse konventionen
*/	
class Konventionen
{
	public static void main(String[] args)
	{
		System.out.println("You will find ");
		System.out.println("Java Code Conventions");
		System.out.println("under \t"); // Position to the next tab
		System.out.println("http://java.sun.com/docs/codeconv/");
	}
}