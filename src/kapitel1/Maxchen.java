package kapitel1;

import java.util.Scanner;

public class Maxchen {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		while (true) {
			// Eingabeaufforderung f�r ersten Wurf
			System.out.println("Ersten Wurf eingeben: ");
			int wurf1 = sc.nextInt();

			int result=0;
			
			// Eingabeaufforderung f�r zweiten Wurf
			System.out.println("Zweiten Wurf eingeben: ");
			int wurf2 = sc.nextInt();
			
			if (wurf1 + wurf2 == 3) {
				System.out.println("M�xchen!\n1000 Punkte");
			}
			else if (wurf1 == wurf2) {
				System.out.println("Pasch!\n" + (wurf1 * 100));
			}
			else {
				if (wurf1 > wurf2) {
					result = (wurf1 * 10) + wurf2;
				}
				else {
					result = (wurf2 * 10) + wurf1;
				}
				System.out.println("Normal..." + result);
			}
		}
	}

}
