package kapitel1;


import java.util.Scanner;

public class Wochentag {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);	// Festlegen eines neuen Scanners

		System.out.println("Tag eingeben");
		int d = sc.nextInt();					// Einlesen Tag
		
		System.out.println("Monat eingeben");
		int m = sc.nextInt(); 					// Einlesen Monat

		System.out.println("Jahr eingeben");
		int c = sc.nextInt();					// Einlesen Jahr
		
		int y = 0;
		
		String tag = "";
		
		if (m == 13){c -= 1;}
		else if (m == 14){c -= 1;}

		y = c%100;								// Jahr im Jahrhundert
		
		int w = (d + ((26*(m+1))/10) + ((5*y)/4) + (c/4) + 5*c - 1)%7;	// Durchf�hren der Rechnung

		// Jeweilige anpassung der Variable tag f�r den jeweiligen Wochentag
		if (w == 1){tag = "Montag";}
		else if (w == 2){tag = "Dienstag";}		
		else if (w == 3){tag = "Mittwoch";}
		else if (w == 4){tag = "Donnerstag";}		
		else if (w == 5){tag = "Freitag";}
		else if (w == 6){tag = "Samstag";}		
		else if (w == 7){tag = "Sonntag";}
		
		System.out.println(w);					// Ausgabe des Ergebnisses
		System.out.println(tag);				// Ausgabe des Wochentages
	}
}
