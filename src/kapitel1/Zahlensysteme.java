//Aufgabe 2.2

package kapitel1;

import java.util.Scanner;


public class Zahlensysteme
{
	public static void main( String[] args)
	{
			Scanner sc = new Scanner(System.in);							// Festlegen eines neuen Scanners

			// Erster Teil
			System.out.println("Bitte geben Sie eine Zahl dezimal ein");	// Frage nach Eingabe einer Ganzzahl
			int dezi = sc.nextInt();										// Eingabe der Ganzzahl

			String hex_wert = Integer.toHexString(dezi);					// Umwandeln des Wertes in einen Hex Wert
			System.out.println("Hex: " + hex_wert);							// Ausgabe des Hex Wertes
			
			String binaer_wert = Integer.toBinaryString(dezi);				// Umwandeln des Wertes in einen Bin�r Wert
			System.out.println("Bin�r: " + binaer_wert);					// Ausgabe des Bin�rwertes


			// Zweiter Teil
			System.out.println("Bitte geben Sie eine Zahl hexadezimal ein");// Frage nach Eingabe einer Hexadezimalzahl
			String Zeichenkette = sc.next();								// Eingabe der Hexadezimalzahl

			dezi = Integer.parseInt(Zeichenkette, 16);						// Umwandeln des Wertes in einen Hex Wert
			System.out.println("Dezimal: " + dezi);							// Ausgabe des Dezimalwertes
			
			binaer_wert = Integer.toBinaryString(dezi);						// Umwandeln des Wertes in einen Bin�r Wert
			System.out.println("Binaer: " + binaer_wert);					// Ausgabe des Bin�rwertes

			
			// Dritter Teil
			System.out.println("Bitte geben Sie eine Zahl bin�r ein");		// Frage nach Eingabe einer bin�ren Zahl
			binaer_wert = sc.next();										// Eingabe der Bin�ren Zahl

			dezi = Integer.parseInt(binaer_wert, 2);						// Umwandeln des Wertes in einen Hex Wert
			System.out.println("Dezimal: " + dezi);							// Ausgabe des Dezimalwertes

			hex_wert = Integer.toHexString(dezi);							// Umwandeln des Wertes in einen binaer Wert
			System.out.println("Binaer: " + hex_wert);						// Ausgabe des Binaerwertes
			
	
			// Vierter Teil
			System.out.println("Bitte geben Sie eine Zahl octal ein");		// Frage nach Eingabe einer Octalen Zahl
			String octal_wert = sc.next();									// Eingabe einer octalen Zahl
			
			dezi = Integer.parseInt(octal_wert, 8);							// Umwandeln des Wertes in einen dezimalen Wert
			System.out.println("Dezimal: " + dezi);							// Ausgabe des dezimalen Wert

			hex_wert = Integer.toHexString(dezi);							// Umwandeln des Wertes in einen hexalen Wert
			System.out.println("Hex: " + hex_wert);							// Ausgabe des hexalen Wertes
			}
}