package kapitel1;

import java.util.Scanner;

public class DreierSumme {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		int sum = 0;
		
		if(n < 3)
			n = 0;
		
		else if(n%3!=0)
			n-=(n%3);
	
// DO-WHILE SCHLEIFE
		int i=3;
		do{
			if(i==n)
				System.out.println(i);
			else
				System.out.print(i + "+");
			sum += i;
			i+=3; //While
		}while(i <= n);
		System.out.println("Summe = " + sum);

		/*
WHILE SCHLEIFE
 		int i=3; 
		while(i <= n) {
			if(i==n)
				System.out.println(i);
			else
				System.out.print(i + "+");
			sum += i;
			i+=3;
		}
		System.out.println("Summe = " + sum);

		 */
		
		/*	
FOR-SCHLEIFE
		for(int i=3; i<=n; i+=3) {
			if(i==n)
				System.out.println(i);
			else
				System.out.print(i + "+");
			sum += i;
		}
		System.out.println("Summe = " + sum);
		 */
	sc.close();
	}
}
